var express = require('express')
var app = express()
const students = ["Valentin", "Lucas", "Raph","Tam","Max"]



app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*")
    res.setHeader("Access-Control-Allow-Headers", "*")
    next()
})

app.get("/",(req,res)=> {
    res.send("Home")
})

app.get("/students",(req,res) => {
    res.send(students)
})

app.post("/student",(req,res) => {
    students.push(req.body.name);
    res.sendStatus(res.statusCode)
    console.log(students)
})

app.listen(666)